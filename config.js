'use strict';

const fs = require('fs');

/*
Read a default config file, then check if an additional config file exists and overwrite the default settings if it does.
*/
let Config = JSON.parse(fs.readFileSync(__dirname+'/config/config.default.json'));

if (fs.existsSync(__dirname+'/config/config.json')) {
    Config = Object.assign(Config,JSON.parse(fs.readFileSync(__dirname+'/config/config.json')));
}

module.exports = Config;