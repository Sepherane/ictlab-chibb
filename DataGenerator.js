'use strict';

const request = require('request');

let sensors = [
    {
        name: "GeneratedSensor",
        MINVALUE: 15,
        MAXVALUE: 24,
        interval: 60 * 1000,
        oldValue: 20,
    },
    {
        name: "HumidSensor",
        MINVALUE: 6,
        MAXVALUE: 12,
        interval: 60 * 1000,
        oldValue: 10,
    }
];

const DataGenerator = {
    server_address: "http://146.185.142.242/",
    init: function () {
        sensors.forEach(function (sensor) {
            DataGenerator.sendQuery(sensor);
        });
    },
    sendQuery: function (sensor) {
        sensor.oldValue = this.generateValue(sensor);
        request.post(
            this.server_address + 'api/measurements',
            {json: {name: sensor.name, values: sensor.oldValue}},
            function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    //Success
                }
            }
        );

        setTimeout(this.sendQuery.bind(this), sensor.interval, sensor);
    },
    generateValue(sensor){
        let oldValue = sensor.oldValue;

        let volatility = Math.random() * 4 + 2;

        let rnd = Math.random();

        let changePercent = volatility * rnd;

        if (changePercent > volatility) {
            changePercent -= (2 * volatility);
        }
        let changeAmount = oldValue * changePercent / 100;
        let newValue = oldValue + changeAmount;

        // Add a ceiling and floor.
        if (newValue < sensor.MINVALUE) {
            newValue += Math.abs(changeAmount) * 2;
        } else if (newValue > sensor.MAXVALUE) {
            newValue -= Math.abs(changeAmount) * 2;
        }

        return newValue;
    }
};

module.exports = DataGenerator;