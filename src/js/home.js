'use strict';

//Set the Chart.js defaults to
Chart.defaults.global.tooltipYPadding = 16;
Chart.defaults.global.tooltipCornerRadius = 0;
Chart.defaults.global.responsive = true;
Chart.defaults.global.scaleShowVerticalLines = false;
Chart.defaults.global.legend.display = false;

let dataLabels = {
    "temperature": "Temperature",
    "humidity": "Humidity"
};

class LineChart {

    constructor(canvasId) {
        this.canvas = document.getElementById(canvasId).getContext("2d");
        this.dataType = 'temperature';
        this.timestep = 'minute';
    };

    setTimestep(data) {
        this.timestep = data;
        this.update();
    }

    setDataType(data) {
        this.dataType = data;
        this.update();
    }

    formatDate(date) {
        let timestep = this.timestep;

        if (timestep === 'minute')
            return moment(date).format("HH:mm");
        else if (timestep === 'hour')
            return moment(date).format("DD/MM HH:mm");
        else if (timestep === 'day')
            return moment(date).format("D/M");
        else if (timestep === 'month')
            return moment(date).format("M/YYYY");
        else
            return moment(date).format("HH:mm");
    };

    update() {
        let that = this;
        $.get('/api/measurements/' + this.dataType + '/' + this.timestep).done(function (results) {
            let labels = [];
            let tempData = [];
            for (let i = 0; i < results.length; i++) {
                if (results[i].values.hasOwnProperty(that.dataType)) {
                    labels.push(results[i].timestamp);
                    tempData.push(Math.round((results[i].values[that.dataType].total / results[i].values[that.dataType].count) * 10) / 10)
                }
            }

            let data = {
                labels: labels,
                datasets: [
                    {
                        label: dataLabels[that.dataType],
                        backgroundColor: "rgba(160,0,0,0.2)",
                        borderColor: "rgba(220,0,0,0.5)",
                        pointBackgroundColor: "rgba(255,0,0,0.3)",
                        pointBorderColor: "rgba(160,0,0,0.1)",
                        pointHoverBackgroundColor: "#f00",
                        data: tempData,
                        lineTension: 0
                    }
                ]
            };

            let opts = {
                scales: {
                    xAxes: [{
                        type: "time",
                        time: {
                            displayFormats: {
                                second: "HH:mm",
                                minute: "HH:mm",
                                hour: "DD/MM HH:mm"
                            }
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        title: function (tooltipItem) {
                            return that.formatDate(tooltipItem[0].xLabel);
                        },
                    }
                }
            };
            if (that.chart) {
                that.chart.data = data;
                that.chart.update();
            }
            else
                that.chart = new Chart(that.canvas, {type: "line", data: data, options: opts});

            clearTimeout(that.timeout);
            if (that.timestep === "minute")
                that.timeout = setTimeout(that.update.bind(that), 60000);
        });
    };
}