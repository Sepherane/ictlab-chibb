'use strict';

class Admin {

    //Retrieves a sensor from the list of sensors, returns null if it is not found, the sensor if it is.
    static getSensorById(id) {
        for (let i = 0; i < sensors.length; i++) {
            let sensor = sensors[i];
            if (sensor._id === id)
                return sensor;
        }

        return null;
    }

    //Replaces the title of a sensor with a text input, which updates the name of the sensor when the text changes.
    static changeTitle(sensorId) {
        let sensor = Admin.getSensorById(sensorId);
        if (sensor === null)
            return;

        let titleEditor = $("<input type='text' class='title-editor' value='" + sensor.displayName + "' />");
        titleEditor.change(function () {
            sensor.displayName = $(this).val();
            $('#sensor-' + sensor._id + ' .title-editor').replaceWith("<span class='title-text'>" + sensor.displayName + "</span>");
            Admin.saveSensor(sensor);
        });
        $('#sensor-' + sensor._id + ' .title span.title-text').replaceWith(titleEditor);
    }

    //Changes the enabled status of a sensor, used to determine if it can accept measurements.
    static changeEnabled(sensorId) {
        let sensor = Admin.getSensorById(sensorId);
        if (sensor === null)
            return;

        sensor.enabled = !sensor.enabled;

        Admin.saveSensor(sensor, function () {
            Admin.changeEnabledButton(sensor);
        });

    }

    //Saves an updated sensor in the database
    static saveSensor(sensor, callback) {
        for (let i = sensor.dataTypes.length; i >= 0; i--) {
            if (sensor.dataTypes[i] === 'none')
                sensor.dataTypes.splice(i, 1);
        }
        $.ajax({url: "/api/sensors/" + sensor._id, type: "PUT", data: sensor}).done(callback);
    }

    //Deletes a sensor based on its id
    static deleteSensor(sensorId) {
        $.ajax({url: "/api/sensors/" + sensorId, type: "DELETE"}).done(function () {
            $('#sensor-' + sensorId).remove();
        });
    }

    //Changes the button that shows the enabled status to the correct one.
    static changeEnabledButton(sensor) {
        let buttonElement = $("#sensor-" + sensor._id + " .enablebutton ");
        buttonElement.toggleClass("btn-success btn-danger");
        if (sensor.enabled) {
            buttonElement.html("Enabled");
        }
        else {
            buttonElement.html("Disabled");
        }

    }

    //Updates the data types to the selected ones and saves the sensor.
    static changeDataTypes(sensorId) {
        let sensor = Admin.getSensorById(sensorId);
        if (sensor === null)
            return;

        let dataTypes = [];
        $('#sensor-' + sensorId + ' .sensortype').each(function () {
            dataTypes.push($(this).val());
        });
        sensor.dataTypes = dataTypes;
        Admin.saveSensor(sensor);
    }

    //Add an additional selector for the data types.
    static addTypeSelection(sensorId) {
        let sensorElement = $("#sensor-" + sensorId);

        let newSelect = $("<select></select>").attr('class', 'form-control sensortype').attr('name', 'sensortype');
        [
            {
                "name": "None",
                "value": "none"
            },
            {
                "name": "Temperature",
                "value": "temperature"
            },
            {
                "name": "Humidity",
                "value": "humidity"
            }
        ].forEach(function (val) {
            newSelect.append("<option value='" + val.value + "'>" + val.name + "</option>");
        });
        newSelect.change(function () {
            Admin.changeDataTypes(sensorId)
        });
        newSelect.insertBefore(sensorElement.find('.addnewsensortype'));
    }

    //Adds a floor dropdown and a floor plan canvas to select the position of the sensor
    static locationSelector(sensorId) {
        let sensor = Admin.getSensorById(sensorId);
        let sensorbutton = $("#sensor-" + sensorId + " .edit-location");

        let editor = $("<div></div>").attr('class', 'locationeditor');
        let floorpicker = $("<select></select>").attr('class', 'form-control sensorfloor');
        [
            {
                "name": "Ground floor",
                "value": 0
            },
            {
                "name": "First floor",
                "value": 1
            },
            {
                "name": "Second floor",
                "value": 2
            }
        ].forEach(function (val) {
            floorpicker.append("<option " + (sensor.floor === val.value ? "selected" : "") + " value='" + val.value + "'>" + val.name + "</option>");
        });
        floorpicker.change(function () {
            Admin.changeFloor(sensorId)
        });

        editor.append(floorpicker);

        let floorcanvas = $('<canvas width="800" height="400" class="floorcanvas"></canvas>');

        floorcanvas.click(function (e) {
            Admin.canvasClick(e, sensorId);
        });

        let canvas = floorcanvas[0].getContext("2d");
        let img = new Image();
        img.onload = function () {
            canvas.drawImage(img, 0, 0);
            Admin.canvasClick({offsetX: sensor.position.x, offsetY: sensor.position.y}, sensorId, true);
        };
        img.src = 'http://146.185.142.242/images/floor-' + sensor.floor + '.png';
        editor.append(floorcanvas);
        sensorbutton.replaceWith(editor);
    }


    //Handles a click on the canvas, determining the new coordinates and saving them, then redraws a canvas with a red square at the correct position
    static canvasClick(e, sensorId, forced) {
        forced = forced || false;

        let position = {x: e.offsetX, y: e.offsetY};
        let canvas = $('#sensor-' + sensorId + ' .floorcanvas')[0].getContext('2d');
        let floor = $('#sensor-' + sensorId + " .sensorfloor").val();
        let img = new Image();
        img.src = 'http://146.185.142.242/images/floor-' + floor + '.png';
        canvas.fillStyle = "#FF0000";
        canvas.drawImage(img, 0, 0);
        canvas.fillRect(position.x - 5, position.y - 5, 10, 10);

        let sensor = Admin.getSensorById(sensorId);
        sensor.position = position;
        sensor.floor = floor;

        if (!forced)
            Admin.saveSensor(sensor);
    }

    //Redraws the canvas with a different floor plan
    static changeFloor(sensorId) {
        let floor = $('#sensor-' + sensorId + " .sensorfloor").val();
        let canvas = $('#sensor-' + sensorId + ' .floorcanvas')[0].getContext('2d');
        let img = new Image();
        img.onload = function () {
            canvas.drawImage(img, 0, 0);
        };
        img.src = 'http://146.185.142.242/images/floor-' + floor + '.png';
    }
}