'use strict';

$(function () {
    setCurrentTime();
});

//Set the current time every second
function setCurrentTime() {
    $('.timer').html(moment().format("HH:mm"));
    setTimeout(setCurrentTime, 1000);
}