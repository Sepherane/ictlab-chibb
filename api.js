'use strict';

const Sensor = require('./models/sensor');
const Measurement = require('./models/measurement');
const moment = require('moment');

class API {
    //Searches for a sensor and returns it if found
    static getSensor(sensor) {
        return new Promise(function (fulfill, reject) {
            Sensor.findById(sensor, function (err, sensor) {
                if (err)
                    reject(err);
                fulfill(sensor);
            });
        });
    }

    //Searches for a sensor by name instead of ID.
    static getSensorByName(name) {
        return new Promise(function (fulfill, reject) {
            Sensor.find({'name': name}, function (err, sensor) {
                if (err)
                    reject(err);
                if (Array.isArray(sensor))
                    fulfill(sensor[0]);
                else
                    fulfill(sensor);
            });
        });
    }

    //Updates an existing sensor with the provided information
    static updateSensor(sensor, args) {
        return new Promise(function (fulfill, reject) {
            Sensor.findById(sensor, function (err, sensor) {
                if (err) {
                    reject(err);
                    return;
                }
                sensor.displayName = args.displayName || sensor.displayName;
                sensor.enabled = args.enabled || sensor.enabled;
                sensor.dataTypes = args.dataTypes || sensor.dataTypes;
                sensor.position = args.position || sensor.position;
                sensor.floor = args.floor || sensor.floor;
                sensor.save(function (err) {
                    if (err)
                        reject(err);
                    fulfill({message: 'Sensor updated!'});
                });
            });
        });
    }

    //Removes a sensor completely
    static deleteSensor(sensor) {
        return new Promise(function (reject, fulfill) {
            Sensor.remove({
                _id: sensor
            }, function (err, sensor) {
                if (err)
                    reject(err);

                fulfill({message: 'Successfully deleted'});
            });
        });
    }

    //Adds a new sensor to the page
    static addSensor(args) {
        return new Promise(function (fulfill, reject) {
            let sensor = new Sensor();
            sensor.name = args.name;
            sensor.displayName = sensor.name;

            sensor.save(function (err) {
                if (err)
                    reject(err);

                fulfill({message: 'Sensor created!'});
            });
        });
    }

    //Retrieves all sensors
    static getAllSensors() {
        return new Promise(function (fulfill, reject) {
            Sensor.find(function (err, sensors) {
                if (err)
                    reject(err);

                fulfill(sensors);
            });
        });
    }

    /*
     * Adds a new measurement
     * If the sensor does not exist, it will be created first
     * If the sensor is disabled, it won't go through
     */
    static addMeasurement(args) {
        return new Promise(function (fulfill, reject) {
            API.getSensorByName(args.name).then(function (sensor) {

                // Create sensor if it does not exist
                if (typeof(sensor) === 'undefined') {
                    API.addSensor(args);
                    fulfill({message: "Sensor did not yet exist, made a new one"});
                    return;
                }

                if (!sensor.enabled) {
                    reject({message: "Sensor is disabled"});
                    return;
                }

                if (!sensor.dataTypes || sensor.dataTypes.length === 0) {
                    reject({message: "Sensor does not have any data types"});
                    return;
                }

                if (!Array.isArray(args.values))
                    args.values = [args.values];

                sensor.lastUpdate.interval = (Date.now() - sensor.lastUpdate.date) / 1000;
                sensor.lastUpdate.date = Date.now();

                sensor.lastMeasurement = args.values;

                sensor.save();

                for (let i = 0; i < args.values.length; i++) {
                    let dataType = sensor.dataTypes[i];
                    if (typeof(dataType) === 'undefined')
                        break;

                    API.saveMeasurement(Measurement.minutes, new Date(moment().startOf('minute')), args.values[i], dataType).then(function () {
                        API.saveMeasurement(Measurement.hourly, new Date(moment().startOf('hour')), args.values[i], dataType).then(function () {
                            API.saveMeasurement(Measurement.daily, new Date(moment().startOf('day')), args.values[i], dataType).then(function () {
                                API.saveMeasurement(Measurement.monthly, new Date(moment().startOf('month')), args.values[i], dataType).then(function (response) {
                                    fulfill(response);
                                });
                            });
                        });
                    });
                }
            });
        });
    }

    //Actually saves the measurement to the database. If a measurement already exists, the data is appended.
    static saveMeasurement(measurement, timestamp, value, dataType) {
        return new Promise(function (fulfill, reject) {
            let valuefield = "values." + dataType;
            let query = {'timestamp': timestamp};
            query[valuefield] = {$exists: true, $ne: null};
            measurement.findOne(query, function (err, measure) {
                if (measure === null) {
                    let newmeasurement = new measurement();
                    newmeasurement.timestamp = timestamp;
                    newmeasurement.values[dataType] = {total: value, count: 1};
                    newmeasurement.save(function (err) {
                        if (err)
                            reject(err);
                        fulfill({message: "Measurement saved"});
                    })
                }
                else {
                    if (!measure.values.hasOwnProperty(dataType))
                        measure.values[dataType] = {total: 0, count: 0};

                    measure.values[dataType].total += value;
                    measure.values[dataType].count++;
                    measure.save(function (err) {
                        if (err)
                            reject(err);
                        fulfill({message: "Measurement saved"});
                    })
                }
            });
        });
    }

    //Retrieves measurements based on the type and timeframe
    static getMeasurement(type, timeframe) {
        return new Promise(function (fulfill, reject) {
            let measurement = API.getMeasurementByTimeframe(timeframe);
            let valuefield = "values." + type;
            let query = {timestamp: {$gt: measurement.limit}};
            query[valuefield] = {$exists: true, $ne: null};
            measurement.type.find(query, function (err, measurements) {
                if (err)
                    reject(err);
                fulfill(measurements);
            });
        });
    }

    //Returns the measurement type and the limit for retrieving the measurements based on the timeframe
    static getMeasurementByTimeframe(timeframe) {
        switch (timeframe) {
            case 'day':
            case 'daily':
                return {type: Measurement.daily, limit: new Date(moment().subtract(30, 'days'))};
                break;
            case 'minute':
                return {type: Measurement.minutes, limit: new Date(moment().subtract(120, 'minutes'))};
                break;
            case 'hour':
            case 'hourly':
                return {type: Measurement.hourly, limit: new Date(moment().subtract(72, 'hours'))};
                break;
            case 'month':
            case 'monthly':
                return {type: Measurement.monthly, limit: new Date(moment().subtract(24, 'months'))};
                break;
            default:
                return {type: Measurement.minutes, limit: new Date(moment().subtract(120, 'minutes'))};
                break;
        }
    }
}

module.exports = API;