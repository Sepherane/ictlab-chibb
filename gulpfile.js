'use strict';

const gulp = require('gulp');
const concat = require('gulp-concat');
const minifycss = require('gulp-clean-css');
const rename = require('gulp-rename');
const svgmin = require('gulp-svgmin');

// Concatenate css files
gulp.task('build-css', function () {
    gulp.src('./src/css/**/*.css')
        .pipe(concat('stylesheet.css'))
        .pipe(minifycss())
        .pipe(gulp.dest('./public/css/'));
});

gulp.task('build-js',function(){
    gulp.src('./src/js/**/*.js')
        .pipe(concat('javascript.js'))
        .pipe(gulp.dest('./public/js'));
});

//Move unity build files to the correct place
gulp.task('build-unity',function(){
    gulp.src("./src/model/Build/*.*")
        .pipe(gulp.dest('public/model'));

    gulp.src("./src/model/index.html")
        .pipe(rename("model.html"))
        .pipe(gulp.dest('views/includes'));
});

gulp.task('minify-icons',function(){
    gulp.src("./public/images/icons/*.svg")
        .pipe(svgmin())
        .pipe(gulp.dest("./public/images/icons"));
});


gulp.task('build', ['build-css','build-unity', 'build-js']);

gulp.task('watch', function () {
    gulp.watch('src/css/**/*.css', ['build-css']);
    gulp.watch('src/js/**/*.js', ['build-js']);
    gulp.watch('src/model/**/*.*',['build-unity']);
});