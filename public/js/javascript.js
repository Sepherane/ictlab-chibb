'use strict';

class Admin {

    //Retrieves a sensor from the list of sensors, returns null if it is not found, the sensor if it is.
    static getSensorById(id) {
        for (let i = 0; i < sensors.length; i++) {
            let sensor = sensors[i];
            if (sensor._id === id)
                return sensor;
        }

        return null;
    }

    //Replaces the title of a sensor with a text input, which updates the name of the sensor when the text changes.
    static changeTitle(sensorId) {
        let sensor = Admin.getSensorById(sensorId);
        if (sensor === null)
            return;

        let titleEditor = $("<input type='text' class='title-editor' value='" + sensor.displayName + "' />");
        titleEditor.change(function () {
            sensor.displayName = $(this).val();
            $('#sensor-' + sensor._id + ' .title-editor').replaceWith("<span class='title-text'>" + sensor.displayName + "</span>");
            Admin.saveSensor(sensor);
        });
        $('#sensor-' + sensor._id + ' .title span.title-text').replaceWith(titleEditor);
    }

    //Changes the enabled status of a sensor, used to determine if it can accept measurements.
    static changeEnabled(sensorId) {
        let sensor = Admin.getSensorById(sensorId);
        if (sensor === null)
            return;

        sensor.enabled = !sensor.enabled;

        Admin.saveSensor(sensor, function () {
            Admin.changeEnabledButton(sensor);
        });

    }

    //Saves an updated sensor in the database
    static saveSensor(sensor, callback) {
        for (let i = sensor.dataTypes.length; i >= 0; i--) {
            if (sensor.dataTypes[i] === 'none')
                sensor.dataTypes.splice(i, 1);
        }
        $.ajax({url: "/api/sensors/" + sensor._id, type: "PUT", data: sensor}).done(callback);
    }

    //Changes the button that shows the enabled status to the correct one.
    static changeEnabledButton(sensor) {
        let buttonElement = $("#sensor-" + sensor._id + " .enablebutton ");
        buttonElement.toggleClass("btn-success btn-danger");
        if (sensor.enabled) {
            buttonElement.html("Enabled");
        }
        else {
            buttonElement.html("Disabled");
        }

    }

    //Updates the data types to the selected ones and saves the sensor.
    static changeDataTypes(sensorId) {
        let sensor = Admin.getSensorById(sensorId);
        if (sensor === null)
            return;

        let dataTypes = [];
        $('#sensor-' + sensorId + ' .sensortype').each(function () {
            dataTypes.push($(this).val());
        });
        sensor.dataTypes = dataTypes;
        Admin.saveSensor(sensor);
    }

    //Add an additional selector for the data types.
    static addTypeSelection(sensorId) {
        let sensorElement = $("#sensor-" + sensorId);

        let newSelect = $("<select></select>").attr('class', 'form-control sensortype').attr('name', 'sensortype');
        [
            {
                "name": "None",
                "value": "none"
            },
            {
                "name": "Temperature",
                "value": "temperature"
            },
            {
                "name": "Humidity",
                "value": "humidity"
            }
        ].forEach(function (val) {
            newSelect.append("<option value='" + val.value + "'>" + val.name + "</option>");
        });
        newSelect.change(function () {
            Admin.changeDataTypes(sensorId)
        });
        newSelect.insertBefore(sensorElement.find('.addnewsensortype'));
    }

    //Adds a floor dropdown and a floor plan canvas to select the position of the sensor
    static locationSelector(sensorId) {
        let sensor = Admin.getSensorById(sensorId);
        let sensorbutton = $("#sensor-" + sensorId + " .edit-location");

        let editor = $("<div></div>").attr('class', 'locationeditor');
        let floorpicker = $("<select></select>").attr('class', 'form-control sensorfloor');
        [
            {
                "name": "Ground floor",
                "value": 0
            },
            {
                "name": "First floor",
                "value": 1
            },
            {
                "name": "Second floor",
                "value": 2
            }
        ].forEach(function (val) {
            floorpicker.append("<option " + (sensor.floor === val.value ? "selected" : "") + " value='" + val.value + "'>" + val.name + "</option>");
        });
        floorpicker.change(function () {
            Admin.changeFloor(sensorId)
        });

        editor.append(floorpicker);

        let floorcanvas = $('<canvas width="800" height="400" class="floorcanvas"></canvas>');

        floorcanvas.click(function (e) {
            Admin.canvasClick(e, sensorId);
        });

        let canvas = floorcanvas[0].getContext("2d");
        let img = new Image();
        img.onload = function () {
            canvas.drawImage(img, 0, 0);
            Admin.canvasClick({offsetX: sensor.position.x, offsetY: sensor.position.y}, sensorId, true);
        };
        img.src = 'http://146.185.142.242/images/floor-' + sensor.floor + '.png';
        editor.append(floorcanvas);
        sensorbutton.replaceWith(editor);
    }

    //Handles a click on the canvas, determining the new coordinates and saving them, then redraws a canvas with a red square at the correct position
    static canvasClick(e, sensorId, forced) {
        forced = forced || false;

        let position = {x: e.offsetX, y: e.offsetY};
        let canvas = $('#sensor-' + sensorId + ' .floorcanvas')[0].getContext('2d');
        let floor = $('#sensor-' + sensorId + " .sensorfloor").val();
        let img = new Image();
        img.src = 'http://146.185.142.242/images/floor-' + floor + '.png';
        canvas.fillStyle = "#FF0000";
        canvas.drawImage(img, 0, 0);
        canvas.fillRect(position.x - 5, position.y - 5, 10, 10);

        let sensor = Admin.getSensorById(sensorId);
        sensor.position = position;
        sensor.floor = floor;

        if (!forced)
            Admin.saveSensor(sensor);
    }

    //Redraws the canvas with a different floor plan
    static changeFloor(sensorId) {
        let floor = $('#sensor-' + sensorId + " .sensorfloor").val();
        let canvas = $('#sensor-' + sensorId + ' .floorcanvas')[0].getContext('2d');
        let img = new Image();
        img.onload = function () {
            canvas.drawImage(img, 0, 0);
        };
        img.src = 'http://146.185.142.242/images/floor-' + floor + '.png';
    }
}
'use strict';

$(function(){
    setCurrentTime();
});

//Set the current time every second
function setCurrentTime(){
    $('.timer').html(moment().format("HH:mm"));
    setTimeout(setCurrentTime,1000);
}
'use strict';

Chart.defaults.global.tooltipYPadding = 16;
Chart.defaults.global.tooltipCornerRadius = 0;
Chart.defaults.global.responsive = true;
Chart.defaults.global.scaleShowVerticalLines = false;
Chart.defaults.global.legend.display = false;

let dataLabels = {
    "temperature": "Temperature",
    "humidity": "Humidity"
};

class LineChart {

    constructor(canvasId) {
        this.canvas = document.getElementById(canvasId).getContext("2d");
        this.dataType = 'temperature';
        this.timestep = 'minute';
    };

    setTimestep(data) {
        this.timestep = data;
        this.update();
    }

    setDataType(data) {
        this.dataType = data;
        this.update();
    }

    formatDate(date) {
        let timestep = this.timestep;

        if (timestep === 'minute')
            return moment(date).format("HH:mm");
        else if (timestep === 'hour')
            return moment(date).format("DD/MM HH:mm");
        else if (timestep === 'day')
            return moment(date).format("D/M");
        else if (timestep === 'month')
            return moment(date).format("M/YYYY");
        else
            return moment(date).format("HH:mm");
    };

    update() {
        let that = this;
        $.get('/api/measurements/' + this.dataType + '/' + this.timestep).done(function (results) {
            let labels = [];
            let tempData = [];
            for (let i = 0; i < results.length; i++) {
                if (results[i].values.hasOwnProperty(that.dataType)) {
                    labels.push(results[i].timestamp);
                    tempData.push(Math.round((results[i].values[that.dataType].total / results[i].values[that.dataType].count) * 10) / 10)
                }
            }

            let data = {
                labels: labels,
                datasets: [
                    {
                        label: dataLabels[that.dataType],
                        backgroundColor: "rgba(160,0,0,0.2)",
                        borderColor: "rgba(220,0,0,0.5)",
                        pointBackgroundColor: "rgba(255,0,0,0.3)",
                        pointBorderColor: "rgba(160,0,0,0.1)",
                        pointHoverBackgroundColor: "#f00",
                        data: tempData,
                        lineTension: 0
                    }
                ]
            };

            let opts = {
                scales: {
                    xAxes: [{
                        type: "time",
                        time: {
                            displayFormats: {
                                second: "HH:mm",
                                minute: "HH:mm",
                                hour: "DD/MM HH:mm"
                            }
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        title: function (tooltipItem) {
                            return that.formatDate(tooltipItem[0].xLabel);
                        },
                    }
                }
            };
            if (that.chart) {
                that.chart.data = data;
                that.chart.update();
            }
            else
                that.chart = new Chart(that.canvas, {type: "line", data: data, options: opts});

            clearTimeout(that.timeout);
            if (that.timestep === "minute")
                that.timeout = setTimeout(that.update.bind(that), 60000);
        });
    };
}