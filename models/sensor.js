'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let SensorSchema = new Schema({
    name: String,
    displayName: String,
    enabled: {default: false, type: Boolean},
    lastUpdate: {date: {type: Date, default: Date.now}, interval: {type: Number, default: 0}},
    dataTypes: [String],
    lastMeasurement: [],
    position: {x: {type: Number, default: 0}, y: {type: Number, default: 0}},
    floor: {type: Number, default: 0}
});

module.exports = mongoose.model('Sensor', SensorSchema);