'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const measurementSchema = {
    timestamp: Date,
    values: {temperature: {total: Number, count: Number}, humidity: {total: Number, count: Number}}
};

//Different measurement types, but the format stays the same.
const Measurement = {
    minutes: mongoose.model("measurements.minute", new Schema(measurementSchema)),
    hourly: mongoose.model("measurements.hourly", new Schema(measurementSchema)),
    daily: mongoose.model("measurements.daily", new Schema(measurementSchema)),
    monthly: mongoose.model("measurements.monthly", new Schema(measurementSchema)),
};

module.exports = Measurement;