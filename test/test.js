'use strict';

const assert = require('assert');
const mongoose = require('mongoose');
const conn = mongoose.connect("mongodb://localhost/test");
const API = require("../api");
mongoose.Promise = global.Promise;

const testSensor = {
    name: "Test1",
    values: 20,
    dataTypes: ["temperature"],
    displayName: "Test1",
    enabled: true
};

let sensorID = "";

after(
    function () {
        conn.connection.db.dropDatabase();
    }
);

describe("Database", function () {
    it("Saving a sensor", function () {
        return API.addMeasurement(testSensor).then(function (error) {
            assert(true);
        }).catch(function (error) {
            new Error(error);
        });
    });

    it("Retrieving a sensor", function () {
        return API.getAllSensors().then(function (sensors) {
            sensorID = sensors[0]._id;
            assert.equal(sensors.length, 1);
        }).catch(function (error) {
            new Error(error);
        });
    });

    it("Editing a sensor", function () {
        return API.updateSensor(sensorID, testSensor).then(function () {
            assert(true);
        }).catch(function (error) {
            new Error(error);
        });
    });

    it("Saving a measurement", function () {
        return API.addMeasurement(testSensor).then(function () {
            assert(true);
        }).catch(function (error) {
            new Error(error);
        });
    });

    it("Retrieving a measurement", function () {
        return API.getMeasurement("temperature", "minute").then(function (measurements) {
            assert.equal(measurements.length, 1);
        }).catch(function (error) {
            new Error(error);
        });
    })
});