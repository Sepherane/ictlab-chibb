'use strict';
const express = require('express');
const app = express();
const API = require('./api');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const DataGenerator = require('./DataGenerator');
const Config = require('./config');
const port = Config["port"];
const router = express.Router();
const moment = require('moment');
const session = require('client-sessions');

mongoose.connect(Config["connectionString"]);
mongoose.Promise = global.Promise;

app.use(express.static(__dirname + '/public'));
app.use(function (req, res, next) {
    if (req.headers["user-agent"] === "okhttp/3.2.0")
        res.status(403).send('Forbidden user-agent');
    else
        next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

/* Session helper functions */
app.use(session({
    cookieName: 'session',
    secret: Config["cookie_secret"],
    duration: 30 * 24 * 60 * 60 * 1000,
    activeDuration: 5 * 60 * 1000,
    httpOnly: true
}));

app.use(function (req, res, next) {
    if (req.session && req.session.user) {
        req.user = req.session.user;
        next();
    } else {
        next();
    }
});

//Ensures that the user is logged in when that is required. Redirects to the login page otherwise.
function requireLogin(req, res, next) {
    if (!req.user) {
        res.redirect('/login');
    } else {
        next();
    }
}
/* Session helper functions end */

DataGenerator.init();

/* BEGIN MAIN SITE */
app.set('view engine', 'pug');
app.set('views', __dirname + '/views');

app.get('/', function (req, res) {
    res.render('index', {page: "Dashboard"});
});

app.get('/house', function (req, res) {
    res.render('house', {page: "House"});
});

app.get('/admin', requireLogin, function (req, res) {
    API.getAllSensors().then(function (resp) {
        res.render('admin', {sensors: resp, page: "Admin"});
    });
});

app.post('/login', function (req, res) {
    //Performs a login action if the username and password are correct
    if (req.body.user === Config["admin_user"] && req.body.password === Config["admin_password"]) {
        req.session.user = "admin";
        res.redirect('/admin');
    }
    else
        res.render('login', {error: 'Invalid user or password.', page: "Login"});
});

app.get('/login', function (req, res) {
    res.render('login', {page: "Login"});
});

/* END MAIN SITE */

/* BEGIN API */

router.get('/', function (req, res) {
    res.render('api');
});

router.route('/sensors').post(requireLogin, function (req, res) {
    API.addSensor(req.body).then(function (response) {
        res.json(response);
    });
}).get(function (req, res) {
    API.getAllSensors().then(function (response) {
        res.json(response);
    });
});

router.route('/sensors/:sensor_id').get(function (req, res) {
    API.getSensor(req.params["sensor_id"]).then(function (response) {
        res.json(response);
    });
}).put(requireLogin, function (req, res) {
    API.updateSensor(req.params["sensor_id"], req.body).then(function (response) {
        res.json(response);
    });
}).delete(requireLogin, function (req, res) {
    API.deleteSensor(req.params["sensor_id"]).then(function (response) {
        res.json(response);
    }).catch(function (response) {
        res.json(response);
    });
});

router.route('/measurements').post(function (req, res) {
    API.addMeasurement(req.body).then(function (response) {
        res.json(response);
    }).catch(function (response) {
        res.json(response);
    });
}).get(function (req, res) {
    API.getMeasurement('temperature', 'minute').then(function (response) {
        res.json(response);
    });
});

router.route('/measurements/:type').get(function (req, res) {
    API.getMeasurement(req.params["type"], "minute").then(function (response) {
        res.json(response);
    });
});

router.route('/measurements/:type/:timeframe').get(function (req, res) {
    API.getMeasurement(req.params["type"], req.params["timeframe"]).then(function (response) {
        res.json(response);
    });
});

app.use('/api', router);

/* API END */

app.listen(port);

console.log("App started on port " + port);