# API documentation

## Sensor data

### Retrieving all sensors

```
GET /api/sensors
```
Returns a list of all sensors, both activated and deactivated.

Example response:
```
[{
    "_id": "5921bae8c12947200d26fc1c",
    "name": "GeneratedSensor",
    "displayName": "Temperature",
    "floor": 0,
    "position": {
        "y": 129,
        "x": 640
    },
    "lastMeasurement": [
        22
    ],
    "dataTypes": [
        "temperature"
    ],
    "lastUpdate": {
        "interval": 60.004,
        "date": "2017-06-11T18:02:23.615Z"
    },
    "enabled": true
}]
```

### Adding a sensor

```
POST /api/sensors
```

This creates a new sensor, using the previously mentioned properties, all optional. Requires you to be logged in.

### Retrieving a single sensor

```
GET /api/sensors/:id
```

Returns the same information as the list of all sensors, but for a single one.

### Updating a sensor

```
PUT /api/sensors/:id
```

Updates a sensor with whatever information you provide, information not provided is ignored. The name of a sensor can't be changed.
Requires you to be logged in.

### Deleting a sensor

```
DELETE /api/sensors/:id
```

Deletes a specified sensor entirely, by its id. Requires you to be logged in.

## Measurement data

### Adding a new measurement

```
POST /api/measurements
```

Adds a new measurement. Requires the name of the sensor and a measurement or an array of measurements.
If the sensor is not yet known, it is added.

Expected data:
```
{
    "name": "Testsensor",
    "measurement":22
}
```

### Retrieving measurements

```
GET /api/measurements/:type/:timestep
```

Retrieves all measurements beloning to a type and a timestep. Valid types are *temperature* and *humidity*.
Valid timesteps are *minute*, *hour*, *day* and *month*. Default type is *temperature* and the default timestep is *minute*.

Example response:

```
{
    "_id": "593d6d8b7a13f5432f43af9c",
    "timestamp": "2017-06-11T16:19:00.000Z",
    "values": {
        "temperature": {
            "total": 22,
            "count": 1
        }
    }
```